package ExcelFileUtility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReadWrite {

	public static void main(String[] ar) throws IOException {

		// Create and object of file class to open xlsx file

		File fil = new File("C:\\Users\\dell\\eclipse-workspace\\AutomationSelenium\\testdata.xlsx");

		// create an object of FileInputStream

		FileInputStream fis = new FileInputStream(fil);
		Workbook wb = null;
		Sheet sh = null;
		wb = new XSSFWorkbook(fis);		
		sh = wb.getSheet("Sheet1");
		int rowCnt = sh.getLastRowNum() - sh.getFirstRowNum();
		System.out.println(rowCnt);		
		
		Row r = sh.getRow(1);
		String s = r.getCell(0).getStringCellValue();
		
		System.out.println(r.getCell(0).getCellType());
		
		System.out.println(s);
		
		fis.close();
		

	}

}
