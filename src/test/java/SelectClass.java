import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SelectClass {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver;
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\dell\\eclipse-workspace\\AutomationSelenium\\src\\test\\resources\\chromedriver.exe");

		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.seleniumeasy.com/test/basic-select-dropdown-demo.html");
		
		WebElement we = driver.findElement(By.id("select-demo"));
		
		
		
		Select sel = new Select(we);
		
		sel.selectByValue("Tuesday");
		
		WebElement str = sel.getFirstSelectedOption();
		System.out.println(str.getText());
		
		
		List<WebElement>lst = sel.getOptions();
		
		for(int i=0;i<lst.size();i++) {		
			System.out.println(lst.get(i).getText());
		}
		
		WebElement weMulti = driver.findElement(By.id("multi-select"));
		
		 sel = new Select(weMulti);
		sel.selectByIndex(0);
		sel.selectByIndex(1);
		
		Thread.sleep(4000);
		
		sel.deselectByIndex(1);
		
		
	}

}
