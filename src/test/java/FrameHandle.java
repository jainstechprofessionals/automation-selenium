import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FrameHandle {

	public static void main(String[] args) {
		WebDriver driver;
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\dell\\eclipse-workspace\\AutomationSelenium\\src\\test\\resources\\chromedriver.exe");

		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://jqueryui.com/accordion/");
		
		WebElement we = driver.findElement(By.xpath("//iframe[@class='demo-frame']"));
		
		
		List<WebElement> le = driver.findElements(By.tagName("iframe"));	
		
		
		System.out.println(le.size());
		
		
		
		driver.switchTo().frame(we);
		

		driver.findElement(By.xpath("//h3[text()='Section 4']")).click();
	}

}
