package TestNGPractice;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ParameterPassing {
 
	
	
  @Test
  @Parameters("browser")
  public void f(@Optional("safari") String username ) {
	  System.out.println(username);
  }
  
  
  
  
}
