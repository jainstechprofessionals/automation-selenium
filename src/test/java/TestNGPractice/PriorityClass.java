package TestNGPractice;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PriorityClass {
	
		
	
	
	@Test (priority=6,invocationCount = 4)
	public void openBrowser() {
		System.out.println("open browser");
		Assert.assertTrue(false,"test case failed: due ............");
		System.out.println("hello");
	}
	
  @Test (priority = 0,description = "this test is for login")
  public void login() {
	  System.out.println("testcase1");
  }
  
  
  
  
  //@Test(priority = 1,dependsOnMethods = {"login","openBrowser"},alwaysRun = true)
  public void homePage() {
	  System.out.println("testcase2");
  }
  @Test(priority = 2,dependsOnMethods = "login")
  public void registerPage() {
	  System.out.println("testcase3");
  }
  
  
  @Test(priority = 3,dependsOnMethods = "login")
  public void tandom() {
	  System.out.println("in abc");
  }
  
  
}
