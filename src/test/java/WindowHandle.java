import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandle {

	public static void main(String[] args) {
		WebDriver driver;
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\dell\\eclipse-workspace\\AutomationSelenium\\src\\test\\resources\\chromedriver.exe");

		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.seleniumeasy.com/test/window-popup-modal-demo.html");
		
		
		driver.findElement(By.xpath("//a[contains(text(), 'Follow Twitter & Facebook')]")).click();
		
		String parent =driver.getWindowHandle();
		System.out.println(parent);
		Set<String> se = driver.getWindowHandles();
		
		System.out.println(se.size());
		
		Iterator<String> it = se.iterator();
		
		while(it.hasNext()) {
			String child = it.next();
			driver.switchTo().window(child);
			String s = driver.getCurrentUrl();
			System.out.println(child);
//			if(s.contains("twitter")) {
//				break;
//			}
			
		}
		
		driver.findElement(By.xpath("//input[contains(@name,'username_or_email')]")).sendKeys("jainraunak");
		System.out.println(driver.getCurrentUrl());
		
		driver.switchTo().window(parent);
		System.out.println(driver.getCurrentUrl());
	}

}
