import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Day4 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stu

		WebDriver driver;
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\dell\\eclipse-workspace\\AutomationSelenium\\src\\test\\resources\\chromedriver.exe");

		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.seleniumeasy.com/test/javascript-alert-box-demo.html");

		driver.findElement(By.xpath("(//button[text()='Click me!'])[1]")).click();

		Alert alr = driver.switchTo().alert();
		Thread.sleep(3000);
		String st = alr.getText();
		System.out.println(st);

		alr.accept();

		driver.findElement(By.xpath("(//button[text()='Click me!'])[2]")).click();
		Thread.sleep(3000);
		alr = driver.switchTo().alert();
		st = alr.getText();
		System.out.println(st);

		alr.accept();
		boolean flag = driver.findElement(By.xpath("//p[text()='You pressed OK!']")).isDisplayed();

		if (flag == true) {
			System.out.println(" test case pass");
		} else {
			System.out.println("fail");
		}

		driver.findElement(By.xpath("(//button[text()='Click me!'])[2]")).click();
		Thread.sleep(3000);
		alr = driver.switchTo().alert();
		st = alr.getText();
		System.out.println(st);
		alr.dismiss();
		
		
		driver.findElement(By.xpath("//button[text()='Click for Prompt Box']")).click();
		Thread.sleep(3000);
		alr = driver.switchTo().alert();
		st = alr.getText();
		alr.sendKeys("rnakou");
		alr.accept();
		String str = driver.findElement(By.id("prompt-demo")).getText();
		if(str.contains("rnakou")){
			System.out.println(" test case pass");
		} else {
			System.out.println("fail");
		}

	}

}
